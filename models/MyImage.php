<?php

namespace app\models;



use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;

/**
 * Class Image
 */
class MyImage extends Model
{
    /**
     * @param $source
     * @param $destination
     * @param $newWidth
     * @param $newHeight
     *
     * @return bool
     */
    public static function resizeGif($source, $destination, $newWidth, $newHeight)
    {
        $size = getimagesize($source);

        if (empty($size)) {
            return false;
        }

        $oldWidth = $size[0];
        $oldHeight = $size[1];

        //Создаем изображение, для GIF можно использовать обычную imagecreate, но лучше все таки везде использовать imagecreatetruecolor
        $destinationResource = imagecreatetruecolor($newWidth, $newHeight);

        $sourceResource = imagecreatefromgif($source);
        $transparentSourceIndex = imagecolortransparent($sourceResource);

        //Проверяем наличие прозрачности
        if ($transparentSourceIndex !== -1) {
            $transparentColor = imagecolorsforindex($destinationResource, $transparentSourceIndex);

            //Добавляем цвет в палитру нового изображения, и устанавливаем его как прозрачный
            $transparentDestinationIndex = imagecolorallocate($destinationResource,
                $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']);

            imagecolortransparent($destinationResource, $transparentDestinationIndex);

            //На всякий случай заливаем фон этим цветом
            imagefill($destinationResource, 0, 0, $transparentDestinationIndex);
        }

        //Ресайз
        imagecopyresampled($destinationResource, $sourceResource, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);

        //Сохранение
        imagegif($destinationResource, $destination);

        return true;
    }

    /**
     * @param $source
     * @param $destination
     * @param $newWidth
     * @param $newHeight
     *
     * @return bool
     */
    public static function resizeGifShort($source, $destination, $newWidth, $newHeight)
    {
        $imagine = new Imagine();

        $imagine->open($source)
            ->resize(new Box($newWidth, $newHeight))
            ->save($destination, array('flatten' => false));
        return true;
    }

    /**
     * @param $source
     * @param $destination
     * @param $watermark
     * @return bool
     */
    public static function setWatermarkGif($source, $destination, $watermark)
    {
        $stamp = imagecreatefrompng($watermark);// watermark image
        $im = imagecreatefromgif($source);// source image

        $marge_right = 10;
        $marge_bottom = 10;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);

        // Convert gif to a true color image
        $tmp = imagecreatetruecolor(imagesx($im), imagesy($im));
        $bg = imagecolorallocate($tmp, 255, 255, 255);
        imagefill($tmp, 0, 0, $bg);
        imagecopy($tmp, $im, 0, 0, 0, 0, imagesx($im), imagesy($im));
        $im = $tmp;

        imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

        imagepng($im, $destination);
        imagedestroy($im);

        return true;
    }
}