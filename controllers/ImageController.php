<?php
namespace app\controllers;

use app\models\MyImage;
use Yii;
use yii\web\Controller;

/**
 * Class ImageController
 * @package app\controllers
 */
class ImageController extends Controller 
{
    /**
     * Для теста изменения размера gif
     */
    public function actionResizeGif()
    {
        $res = MyImage::resizeGif("./Image/animeTest.gif", "./Image/animeResult.gif", 260, 300);
        if ($res) {
            echo "Resize ok";
        }  else {
            echo "Resize error";
        }
    }

    /**
     * Для теста вставки ватермарки в gif
     */
    public function actionWatermarkGif()
    {
        $res = MyImage::setWatermarkGif("./Image/animeTest.gif", "./Image/animeResultWt.gif", "./Image/sert_2.png");
        if ($res) {
            echo "Resize ok";
        }  else {
            echo "Resize error";
        }
    }

}