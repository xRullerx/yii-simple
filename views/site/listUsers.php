<?php

/* @var $users app\models\User */
/* @var $msg string */

use yii\helpers\Url;

$this->title = 'List users';
$this->params['breadcrumbs'][] = $this->title;

    if (!empty($msg)) {
        echo '<div class="user-success"><h2>'. $msg .'</h2></div>';
    }

    if (empty($users)) {
        echo '<h1>Users not found</h1>';
        die;
    }
?>

<div class="site-list-users">
    <h1>Select user to remove</h1>
    <div>
        <?php
            foreach ($users as $item) {
                if ($item->username == 'admin') {continue;};

                echo '<div class="item-user">';
                $url = Url::to(['site/list-users', 'id' => $item->id]);
                echo '<a href="'. $url . '">'. $item->username . '</a>';
                echo '</div>';
            }
        ?>
    </div>
</div>