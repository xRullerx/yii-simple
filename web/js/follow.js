$(document).ready(function() {
    var elements = [];      //массив жлементов

    // будем собирать только кнопки
    $(".btn")
        .mouseenter(function() {

            // добавляем элемент над мышью в список перемещаемых
            var el = [];

            el['parent'] = ($(this)).parent();
            el['move'] = $(this);

            elements.push(el);

            $(document.body.container).append($(this));
        });

    $(document).mousemove(function(e)
    {
        // перемещаем все элементы
        if (elements.length > 0) {
            $.each(elements, function(index, el){
                el['move'].offset({ left: e.pageX + 5, top: e.pageY - 25 });
            });
        }
    });

    $(document).mousedown(function()
    {
        // сбрасываем список элементов
        if (elements.length > 0) {
            $.each(elements, function(index, el){
                el['parent'].append( el['move'].css({ 'top': 0, 'left': 0 }));
            });
            elements = [];
        }
    });

});
